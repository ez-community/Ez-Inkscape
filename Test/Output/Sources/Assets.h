//
// Generate by EzInkscape
// Language: m
// Date: 30/08/2014 23:04:44
//

@interface Assets : NSObject

// Menu
extern const NSString *kBackgroundMenu;
extern const float kBackgroundMenu_X;
extern const float kBackgroundMenu_Y;

extern const NSString *kButtonStart;
extern const float kButtonStart_X;
extern const float kButtonStart_Y;

// Logo
extern const NSString *kLogo;
extern const float kLogo_X;
extern const float kLogo_Y;

// Objects
extern const NSString *kObjectA;
extern const float kObjectA_X;
extern const float kObjectA_Y;

extern const NSString *kObjectB;
extern const float kObjectB_X;
extern const float kObjectB_Y;

extern const NSString *kObjectC;
extern const float kObjectC_X;
extern const float kObjectC_Y;

extern const NSString *kObjectD;
extern const float kObjectD_X;
extern const float kObjectD_Y;

@end
